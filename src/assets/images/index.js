import logo_vertical from './logo_vertical.png';
import tela_login_bg from './tela_login_bg.jpg';
import einstein_to_text from './einstein_to_text.jpg';
import einstein_floripa_text from './einstein_floripa_text.png';
import icon_for_site from './icon_for_site.png';
import spoiler_alert from './spoiler_alert.jpg';

export {
    logo_vertical,
    tela_login_bg,
    einstein_to_text,
    einstein_floripa_text,
    icon_for_site,
    spoiler_alert
}