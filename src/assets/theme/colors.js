const colors = { 
    primary_blue: '#1D74A7',
    secondary_blue: '#2CA5B2',
    secondary_purple: '#9C6FA8',
}

export default colors