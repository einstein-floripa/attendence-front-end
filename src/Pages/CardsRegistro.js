import React from 'react';
import { Tabs } from 'antd';
import { CardScanner, CardManual } from '../Components';
import CadastroUsuario from './CadastroUsuario';
import { matrixUser } from '../index';
import Registros from './Registros';
export default class CardsRegistro extends React.Component {
    state = { activePage: '1' };

    render() {
        const { activePage } = this.state;
        return (
            <Tabs defaultActiveKey={activePage} onChange={(active) => this.setState({ activePage: active })}>
                <Tabs.TabPane tab='Automático' key='1'>
                    <CardScanner active={activePage === '1'} />
                </Tabs.TabPane>
                <Tabs.TabPane tab='Manual' key='2'>
                    <CardManual active={activePage === '2'} />
                </Tabs.TabPane>
                {matrixUser() && <Tabs.TabPane tab='Cadastro' key='3'>
                    <CadastroUsuario active={activePage === '3'} />
                </Tabs.TabPane>}
                {matrixUser() && <Tabs.TabPane tab='Registros' key='4'>
                    <Registros active={activePage === '4'} />
                </Tabs.TabPane>}
            </Tabs>
        );
    }
}