import React from 'react';
import { Form, Icon, Input, InputNumber as InputNumberAntd, Button, notification } from 'antd';
import MaskInput from 'react-maskinput';
import themeColors from '../assets/theme/colors';
import { logo_vertical } from '../assets/images';
import { persistUser } from '../Providers/LoginProvider';
import styled from 'styled-components';

const InputNumber = styled(InputNumberAntd)`
    .ant-input-number-handler-wrap {
        display: none
    }
`



class LoginPage extends React.Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log('Received values of form: ', values);
            if (!err) {
                const payload = {
                    user_name: values.user_name,
                    cpf: values.cpf.replace(/[\.-]/g, ''),
                    password: window.btoa(values.password),
                }
                persistUser(payload).then(resp =>
                    resp.erro ?
                        notification.error({ message: "Erro", description: resp.mensagem }) :
                        notification.success({ message: "Sucesso", description: resp.mensagem }));
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div style={{ textAlign: "center" }}>
                <div style={{ display: 'inline-block' }} >
                    <Form onSubmit={this.handleSubmit} style={{ maxWidth: '300px' }}>
                        <Form.Item label="Nome">
                            {getFieldDecorator('user_name', {
                                rules: [{ required: true, message: 'Por favor insira o novo usuário!' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: themeColors.secondary_blue }} />} placeholder="Usuário" />
                            )}
                        </Form.Item>
                        <Form.Item label="CPF">
                            {getFieldDecorator('cpf', {
                                rules: [{
                                    required: true, message: 'Por favor insira o CPF do novo usrário!'
                                }, {
                                    validator: (rule, value, callback) => {
                                        console.log(value)
                                        if (value.match(/[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}/)) {
                                            callback();
                                        }
                                        callback("CPF inválido!");
                                    }
                                }],
                            })(
                                <MaskInput mask="000.000.000-00" className={"ant-input"} style={{ width: "100%" }} prefix={<Icon type="idcard" style={{ color: themeColors.secondary_blue }} />} placeholder="123.456.789-09" />
                            )}
                        </Form.Item>
                        <Form.Item label="Confirme o CPF">
                            {getFieldDecorator('confirm_cpf', {
                                rules: [{
                                    required: true, message: 'Por favor confirme o CPF do novo usrário!',
                                }, {
                                    validator: (rule, value, callback) => {
                                        const { getFieldValue } = this.props.form
                                        if (value && value !== getFieldValue('cpf')) {
                                            callback('CPF não coincide！');
                                        }
                                        callback();
                                    }
                                }
                                ],
                            })(
                                <MaskInput mask="000.000.000-00" className={"ant-input"} style={{ width: "100%" }} prefix={<Icon type="idcard" style={{ color: themeColors.secondary_blue }} />} placeholder="123.456.789-09" />
                            )}
                        </Form.Item>
                        <Form.Item label="Senha">
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Por favor insira a nova senha!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: themeColors.secondary_blue }} />} type="password" placeholder="Senha" />
                            )}
                        </Form.Item>
                        <Form.Item label="Confirme a senha">
                            {getFieldDecorator('confirm_password', {
                                rules: [{
                                    required: true, message: 'Por favor confirme a nova senha!'
                                }, {
                                    validator: (rule, value, callback) => {
                                        const { getFieldValue } = this.props.form
                                        if (value && value !== getFieldValue('password')) {
                                            callback('Senha não coincide！');
                                        }
                                        callback();
                                    }
                                }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: themeColors.secondary_blue }} />} type="password" placeholder="Senha" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{ width: '100%', backgroundColor: themeColors.primary_blue }}>
                                {'Registrar'}
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        );
    }
}

export default Form.create({ name: 'login' })(LoginPage);
