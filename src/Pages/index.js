import Login from "./Login";
import CardsRegistro from "./CardsRegistro";
import NotFoundPage from "./NotFoundPage";
import CadastroUsuario from "./CadastroUsuario";

export { Login, CardsRegistro, NotFoundPage, CadastroUsuario };
