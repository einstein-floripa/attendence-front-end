import React from "react";
import ReactToPrint from "react-to-print";
import { Table, Icon, Button } from "antd";
import { fetchRegistros } from "../Providers/AlunosProvider";

const sortAlph = (a, b) => {
  if (a.toLowerCase() < b.toLowerCase()) {
    return -1;
  } else if (a.toLowerCase() > b.toLowerCase()) {
    return 1;
  } else {
    return 0;
  }
};

const getData = a => a.replace(/[A-z]/g, " ").split(" ")[0];

const getHora = a => a.replace(/[A-z]/g, " ").split(" ")[1];

const columns = [
  {
    title: "Nome",
    dataIndex: "nome_aluno",
    sorter: (a, b) => sortAlph(a.nome_aluno, b.nome_aluno),
    width: "20%"
  },
  {
    title: "Matrícula",
    dataIndex: "code",
    sorter: (a, b) => console.log(a.code - b.code) || a.code - b.code,
    width: "20%"
  },
  {
    title: "Estado",
    dataIndex: "description",
    sorter: (a, b) => sortAlph(a.description, b.description),
    width: "20%"
  },
  {
    title: "Ação",
    children: [
      {
        title: "Data",
        dataIndex: "action_timestamp",
        render: text => getData(text),
        sorter: (a, b) =>
          sortAlph(getData(a.action_timestamp), getData(b.action_timestamp)),
        width: "10%"
      },
      {
        title: "Hora",
        dataIndex: "action_timestamp",
        render: text => getHora(text),
        sorter: (a, b) =>
          sortAlph(getHora(a.action_timestamp), getHora(b.action_timestamp)),
        width: "10%"
      }
    ]
  },
  {
    title: "Registro",
    children: [
      {
        title: "Data",
        dataIndex: "register_timestamp",
        render: text => getData(text),
        sorter: (a, b) =>
          sortAlph(
            getData(a.register_timestamp),
            getData(b.register_timestamp)
          ),
        width: "10%"
      },
      {
        title: "Hora",
        dataIndex: "register_timestamp",
        render: text => getHora(text),
        sorter: (a, b) =>
          sortAlph(
            getHora(a.register_timestamp),
            getHora(b.register_timestamp)
          ),
        width: "10%"
      }
    ]
  }
];

export default class Registros extends React.PureComponent {
  state = { registros: [] };

  componentDidMount() {
    fetchRegistros().then(registros => {
      console.log(registros);
      /* 
            const filtersRegisterData = registros === null ? [] : Array.from(new Set(
                registros.map(d => d.register_timestamp),
            )).sort((a, b) => getData(a) - getData(b)).map(c => ({ text: c, value: c }));
             */
      this.setState({ registros });
    });
  }

  render() {
    return (
      <div>
        <ReactToPrint
          trigger={() => (
            <Button style={{ margin: 10, marginTop: 0 }}>
              Imprimir <Icon type="printer" theme="twoTone" />
            </Button>
          )}
          content={() => this.componentRef}
        />
        <Table
          size="small"
          ref={el => (this.componentRef = el)}
          pagination={{ pageSize: 10 }}
          columns={columns}
          dataSource={this.state.registros}
        />
      </div>
    );
  }
}
