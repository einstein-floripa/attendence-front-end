import React from 'react';
import { Form, Icon, Input, Button, notification } from 'antd';
import themeColors from '../assets/theme/colors';
import { logo_vertical } from '../assets/images';
import { fetchNewLogin } from '../Providers/LoginProvider';
class LoginPage extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log('Received values of form: ', values);
      if (!err) {
        values.password = window.btoa(values.password);
        this.login(values);
      }
    });
  }

  login = async (params) => {
    const { history } = this.props;
    const json = await fetchNewLogin(params);
    console.log(json)
    if (json && json.token) {
      console.log('token', json.token);
      localStorage.setItem('tokeninstein', json.token);
      console.log('local', localStorage.getItem('tokeninstein'))
      history.push({ pathname: '/chamada' });
    } else {
      notification.warning({
        message: 'Usuário e/ou Senha inválidos.',
      });
    }
  };


  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div style={{ textAlign: "center" }}>
        <div style={{ display: 'inline-block' }} >
          <Form onSubmit={this.handleSubmit} style={{ maxWidth: '300px' }}>
            <div>
              <img width="100%" alt="Einsten Floripa Vestibulares" src={logo_vertical} style={{ padding: 20 }} />
            </div>
            <Form.Item>
              {getFieldDecorator('user', {
                rules: [{ required: true, message: 'Por favor insira o seu usuário!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: themeColors.secondary_blue }} />} placeholder="Usuário" />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Por favor insira a sua senha!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ color: themeColors.secondary_blue }} />} type="password" placeholder="Senha" />
              )}
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" style={{ width: '100%', backgroundColor: themeColors.primary_blue }}>
                {'Entrar'}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

export default Form.create({ name: 'login' })(LoginPage);
