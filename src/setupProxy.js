const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(proxy("/internal", { target: "http://192.168.1.114:8000/" }));
  /* app.use(
    proxy("/apichamada", {
      target: "https://chamada-back-end.herokuapp.com/"
    })
  ); */
};
