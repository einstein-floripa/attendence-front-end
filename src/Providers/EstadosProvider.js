import BaseProvider from "./BaseProvider";

export const getEstados = values => {
  const url = "/apichamada/fetchestados";
  return BaseProvider.GET(url);
};
