import moment from 'moment'
import BaseProvider from './BaseProvider'

export const persistRegistro = (values) => {
    const url = '/apichamada/registro';
    const params = {
        code: values.matricula,
        action_timestamp: values.data && values.hora ? `${values.data.format('YYYY-MM-DD')} ${values.hora.format('HH:mm:ss')}` : moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
        register_timestamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
        id_state: values.estado
    }

    return BaseProvider.POST(url, params);
}

export const fetchRegistros = () => {
    const url = '/apichamada/registro';
    return BaseProvider.GET(url);
}

export const fetchAluno = (values) => {
    const url = `/apichamada/fetchaluno?codAluno=${values}`;
    return BaseProvider.GET(url);
}