import BaseProvider from './BaseProvider';

export const fetchNewLogin = params => BaseProvider.POST('/apichamada/login', params);

export const persistUser = params => BaseProvider.POST('/apichamada/usuario', params);
