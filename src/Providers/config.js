const ip = process.env.IP_BACKEND;

export default () => ({
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "*",
    Authorization: "Bearer 1449bd3f3b00815aa264d350a44bd53c1850eb99"
  }
});
