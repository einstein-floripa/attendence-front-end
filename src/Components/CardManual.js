import React, { Component } from "react";
import {
  Form,
  Input,
  DatePicker,
  TimePicker,
  Select,
  Card,
  Button,
  Modal,
  Spin
} from "antd";
import { persistRegistro, fetchAluno } from "../Providers/AlunosProvider";
import { getEstados } from "../Providers/EstadosProvider";
import moment from "moment";
class CardManual extends Component {
  constructor(props) {
    super(props);
    this.state = {
      aluno: "",
      estados: [],
      loadingPage: true
    };
  }

  componentDidMount() {
    const { setFieldsValue } = this.props.form;
    getEstados().then(resp => {
      this.setState({ estados: resp, loadingPage: false });
      setFieldsValue({ estado: resp[1].id });
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        persistRegistro(values).then(resp => {
          console.log("resp:", resp);
          this.setState({ aluno: resp[0].nome_aluno });
        });
      }
    });
  };

  consultaAluno = codAluno => {
    fetchAluno(codAluno).then(resp => {
      if (resp[0]) {
        Modal.info({
          title: "Consulta Aluno",
          content: (
            <div>
              <b>{"Matricula: "}</b> {codAluno}
              <br />
              <b>{"Nome: "}</b> {resp[0].nome_aluno}
            </div>
          )
        });
      } else {
        Modal.error({
          title: "Consulta aluno",
          content: (
            <div>
              <b>Aluno não encontrado</b>
            </div>
          )
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { estados, loadingPage } = this.state;

    const estadosOpt = estados.map(e => {
      console.log(e);
      return (
        <Select.Option key={e.id} value={e.id}>
          {e.description}
        </Select.Option>
      );
    });

    if (loadingPage) {
      return (
        <div style={{ textAlign: "center" }}>
          <Spin />
        </div>
      );
    }

    return (
      <Card title={`Aluno: ${this.state.aluno}`}>
        <Form onSubmit={this.handleSubmit}>
          <Form.Item label="Matrícula">
            {getFieldDecorator("matricula", {
              rules: [{ required: true, message: "Insira a matrícula!" }]
            })(
              <Input.Search
                placeholder="Matrícula"
                onSearch={this.consultaAluno}
              />
            )}
          </Form.Item>
          <Form.Item label="Estado">
            {getFieldDecorator("estado", {
              rules: [{ required: true, message: "Selecione o estado!" }]
            })(<Select placeholder="Estado">{estadosOpt}</Select>)}
          </Form.Item>
          <Form.Item label="Data">
            {getFieldDecorator("data", {
              rules: [{ required: true, message: "Determine a data!" }],
              initialValue: moment()
            })(
              <DatePicker
                format="DD/MM/YYYY"
                placeholder="Selecione a data"
                style={{ width: "100%" }}
              />
            )}
          </Form.Item>
          <Form.Item label="Hora">
            {getFieldDecorator("hora", {
              rules: [{ required: true, message: "Determine a hora!" }],
              initialValue: moment()
            })(
              <TimePicker
                format="HH:mm"
                placeholder="Selecione a hora"
                style={{ width: "100%" }}
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              style={{ margin: "auto calc(50% - 45px)" }}
            >
              Registrar
            </Button>
          </Form.Item>
        </Form>
      </Card>
    );
  }
}

export default Form.create({ name: "registro_manual" })(CardManual);
