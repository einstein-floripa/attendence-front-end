import React, { Component } from "react";
import Scanner from "./Scanner";
import { Icon, Card, Select, Form, Spin, notification } from "antd";
import { persistRegistro } from "../Providers/AlunosProvider";
import { getEstados } from "../Providers/EstadosProvider";

class CardScanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      torch: false,
      estados: [],
      aluno: "",
      loadingPage: true
    };
    this._scan = this._scan.bind(this);
    this._onDetected = this._onDetected.bind(this);
  }

  componentDidMount() {
    const { setFieldsValue } = this.props.form;
    getEstados().then(resp => {
      this.setState({ estados: resp, estado: resp[1].id, loadingPage: false });
      setFieldsValue({ estado: resp[1].id });
    });
  }

  _scan() {
    this.setState({ scanning: !this.state.scanning });
  }

  _onDetected(result) {
    if (result.codeResult.code !== this.state.result) {
      this.setState({ result: result.codeResult.code }, () => {
        persistRegistro({
          matricula: this.state.result,
          estado: this.state.estado
        }).then(resp => {
          console.log("resp: ", resp);
          if (resp[0]) {
            this.setState({ aluno: resp[0].nome_aluno });
            notification.success({
              description: "Sucesso",
              message: "Registro inserido",
              duration: 2
            });
          } else {
            notification.error({
              description: "Erro",
              message: "Não foi possível inserir",
              duration: 2
            });
          }
        });
      });
    }
  }
  render() {
    const { estados, aluno, estado, torch, zoom, loadingPage } = this.state;
    const {
      form: { getFieldDecorator },
      active
    } = this.props;

    const estadosOpt = estados.map(e => {
      return (
        <Select.Option key={e.id} value={e.id}>
          {e.description}
        </Select.Option>
      );
    });

    if (loadingPage || !active) {
      return (
        <div style={{ textAlign: "center" }}>
          <Spin />
        </div>
      );
    }

    return (
      <Card
        title={`Aluno: ${aluno}`}
        actions={[
          <Icon
            type="bulb"
            theme={torch ? "filled" : "outlined"}
            onClick={() => this.setState({ torch: !torch })}
          />,
          <Icon
            type="zoom-in"
            onClick={() =>
              this.setState({ zoom: "in" }, () =>
                this.setState({ zoom: undefined })
              )
            }
          />,
          <Icon
            type="zoom-out"
            onClick={() =>
              this.setState({ zoom: "out" }, () =>
                this.setState({ zoom: undefined })
              )
            }
          />
        ]}
      >
        <Form.Item style={{ textAlign: "center" }} label="Estado">
          {getFieldDecorator("estado", {
            rules: [
              {
                required: true,
                message: "Por favor selecione um estado de leitura!"
              }
            ]
          })(
            <Select
              style={{
                width: "50%"
              }}
              placeholder="Estado"
              onSelect={estado => this.setState({ estado })}
              value={estado}
            >
              {estadosOpt}
            </Select>
          )}
        </Form.Item>
        {
          <Scanner
            onDetected={this._onDetected}
            torch={torch}
            zoom={zoom}
            scanning={true}
          />
        }
      </Card>
    );
  }
}

const CardScannerForm = Form.create()(CardScanner);

export default CardScannerForm;
