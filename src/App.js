import React from 'react';

import Login from './Pages/Login';
import CardsRegistro from './Pages/CardsRegistro';



export default class App extends React.Component {
  state = {
    welcome: true
  }

  handleInit = () => {
    this.setState({ welcome: false })
  }

  render() {
    const { welcome } = this.state

    if (welcome) {
      return (
        <Login handleInit={this.handleInit} />
      )
    }
    return (
      <CardsRegistro />
    );
  }
}