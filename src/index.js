import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "antd/dist/antd.css";
import { Login, CardsRegistro, NotFoundPage, CadastroUsuario } from "./Pages";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { LocaleProvider } from "antd";
import pt_BR from "antd/lib/locale-provider/pt_BR";
import moment from "moment";

moment.locale("pt");

export const matrixUser = () => {
  const token = localStorage.getItem("tokeninstein");
  if (!token) return false;
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace("-", "+").replace("_", "/");
  const json = JSON.parse(window.atob(base64));
  console.log("matrixUser: ", json);
  if (json.id === 1) {
    // Admin id
    return true;
  }
  return false;
};

ReactDOM.render(
  <LocaleProvider locale={pt_BR}>
    <BrowserRouter>
      <Switch>
        <Route
          path="/"
          exact={true}
          component={
            localStorage.getItem("tokeninstein")
              ? () => <Redirect to="/chamada" />
              : Login
          }
        />
        <Route
          path="/login"
          exact={true}
          component={
            localStorage.getItem("tokeninstein")
              ? () => <Redirect to="/chamada" />
              : Login
          }
        />
        <Route path="/chamada" exact={true} component={CardsRegistro} />
        <Route
          path="/cadastro"
          exact={true}
          component={matrixUser() ? CadastroUsuario : () => <Redirect to="/" />}
        />
        <Route path="*" component={NotFoundPage} />
      </Switch>
    </BrowserRouter>
  </LocaleProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
